"""Read/write utility functions"""
import os
import joblib
import yaml
from typing import Any

def save_joblib(data : Any, filename : str, overwrite = True, compress = 9, verbose = True) -> None:
    """
    Saves the {data} to the specified .joblib file.

    Parameters:
    -----------
    data : Any
        Any kind of data you want to write.

    filename : str
        Name of the resulting file.

    overwrite : bool
        If file already exists should it be overwritten?

    compress : int
        Compression level.

    verbose : bool
        Whether to print messages.

    Returns:
    -----------
    """
    assert filename.split(".")[-1] == "joblib", "Only .joblib format is supported"

    if os.path.isfile(filename) and not overwrite:
        raise Exception(f"FILE {filename} already exists.")

    joblib.dump(value = data,
                filename = filename,
                compress = compress)

    if verbose:
        print(f"SAVED FILE: {filename}")

def load_joblib(filename : str, verbose = True) -> Any:
    """
    Loads specified .joblib file.

    Parameters:
    -----------
    filename : str
        Name of the file to read.

    verbose : bool
        Whether to print messages.

    Returns:
    -----------
    data : Any
        Any kind of data which was in the specified file.
    """
    assert filename.split(".")[-1] == "joblib", "Only .joblib format is supported"

    data = joblib.load(filename)

    if verbose:
        print(f"FILE {filename} loaded.")

    return data

def load_yaml(filename : str) -> dict:
    """
    Loads specified .yaml/.yml file to a Python dictionary.

    Parameters:
    -----------
    filename : str
        Name of the file to read.

    Returns:
    -----------
    dct : dict
        Dictionary built from the yaml file.
    """
    assert filename.split(".")[-1] in ("yaml, yml"), \
        "Only .yaml/.yml formats are supported"

    with open(filename, "r") as ymlfile:
        dct = yaml.load(ymlfile, Loader = yaml.FullLoader)

    return dct
