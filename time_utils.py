""""Helper functions for timing and timestamps"""
from datetime import datetime
from time import time
from typing import Callable, Any

def current_hour() -> str:
    """Get hour from timestamp"""
    t = datetime.fromtimestamp(time())
    return t.strftime("%H:%M:%S")

def current_date() -> str:
    """Get date from timestamp"""
    t = datetime.fromtimestamp(time())
    return t.strftime("%Y%m%d")

def current_timestamp() -> str:
    """Get formatted timestamp"""
    t = datetime.fromtimestamp(time())
    return t.strftime("%Y%m%d_%H%M%S")

def timer(func : Callable) -> Any:
    """
    Function which measures execution time of other function.
    Meant to be used as a decorator.
    """
    func_name = func.__name__

    def inner(*args, **kwargs):
        print(f"FUNCTION {func_name} STARTED")
        t0 = datetime.now()
        func_result = func(*args, **kwargs)
        t1 = datetime.now()
        duration = t1 - t0
        print(f"FUNCTION {func_name} COMPLETED. DURATION: {duration}")
        
        return func_result
    
    return inner
